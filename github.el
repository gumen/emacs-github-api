(defvar github-last-personal-access-token nil "")
(defvar github-last-base "master" "")
(defvar github-remote "origin" "")

(defun github-create-pr-callback (status pr-title)
  (let* ((res-lines (split-string (buffer-string) "\n"))
         (res-status-line (nth 0 res-lines)))
    (if (string-equal res-status-line "HTTP/1.1 201 Created")

        ;; Pull Request was created successfully
        (let* ((res-raw-body (car (last res-lines)))
               (res-body (json-read-from-string res-raw-body))
               (pr-url (alist-get 'url res-body)))
          (kill-new pr-url)
          (message "GitHub created Pull Request `%s' at: %s (url saved in kill ring)" pr-title pr-url)
          )
          ;; (browse-url pr-url))          ; TODO this is wrong url!

      ;; Failed to create Pull Request
      (pop-to-buffer (buffer-name))
      (message "GitHub failed to create Pull Request `%s'" pr-title))))

(defun github-get-branches (&optional remote)
  "Return list of branches.  If REMOTE is not nil then return
list of all remote branches.  Current branch name will start with
asterisk like usual in git CLI output."
  (split-string (shell-command-to-string
                 (concat "git branch" (if remote " -r" "")))
                "\n" t "\s*"))

(defun github-get-current-branch ()
  (let ((branch (seq-find (apply-partially 'string-match "^\\*\s\\(.*\\)$")
                          (github-get-branches))))
    (match-string 1 branch)))

(defun github-get-remote-branches ()
  (mapcar (lambda (b) (substring b (1+ (length github-remote)) (length b)))
          (seq-filter (apply-partially 'string-match (format "^%s/[[:graph:]]*$" github-remote))
                      (github-get-branches t))))

(defun github-get-last-commit-message (branch)
  (replace-regexp-in-string
   "\n" ""
   (shell-command-to-string
    (format "git log -n 1 --format=%%s %s" branch))))

(defun github-verify-and-execute (command)
  (if (not (commandp command))
      (message "Provided `%S' is not a interactive command" command)
    (if (not (= 0 (call-process "git" nil nil nil "status")))
        (message "You are probably not in git repository")
      (if (not (= 0 (call-process "git" nil nil nil "remote" "get-url" github-remote)))
          (message "No such remote `%s'.  Add it with `git remote add <name> <url>'.
Or use different remote by modifying `github-remote' variable."
                   github-remote)
        (command-execute command)))))

(defun github-create-pr (base head title body &optional personal-access-token)
  ;; TODO Show warning if there are no pushed changes
  (interactive
   (let* ((branches (github-get-remote-branches))

          ;; base
          (default-base (if (seq-contains branches github-last-base)
                            github-last-base
                          (if (seq-contains branches "master")
                              "master" (car branches))))
          (base (completing-read (format "base (%s): " default-base)
                                 branches nil t nil nil default-base))

          ;; head
          (current-branch (github-get-current-branch))
          (default-head (if (seq-contains branches current-branch)
                            current-branch (car branches)))
          (head (completing-read (format "head (%s): " default-head)
                                 branches nil t nil nil default-head))

          ;; title
          (remote-head (format "%s/%s" github-remote head))
          (initial-title (github-get-last-commit-message remote-head))
          (title (read-from-minibuffer "PR title: " initial-title
                                       nil nil nil initial-title))

          ;; body
          (body (read-from-minibuffer "PR body: "))

          ;; Set GitHub Personal Access Token if universal prefix argument
          ;; is present of if token was not yet defined.
          (personal-access-token
           (if (or current-prefix-arg (not github-last-personal-access-token))
               (read-from-minibuffer "Define new GitHub Personal Access Token: ")
             github-last-personal-access-token)))

     (list base head title body personal-access-token)))

  (setq github-last-base base)
  (setq github-last-personal-access-token personal-access-token)

  (setq url-request-method "POST")

  (setq url-request-extra-headers
        `(("User-Agent"    . "emacs") ; Value of this header can be anything
          ("Content-Type"  . "application/json; charset=utf-8")
          ("Authorization" . ,(concat "token " personal-access-token))))

  (setq url-request-data
        (json-encode `(("base"  . ,base)
                       ("head"  . ,head)
                       ("title" . ,title)
                       ("body"  . ,body))))

  (let ((remote-url (replace-regexp-in-string
                     "\n$" ""
                     (shell-command-to-string
                      (format "git remote get-url %s" github-remote)))))

    (if (not (string-match "^git@.+:\\(.*\\)\\.git$" remote-url))
        (message "Couldn't find owner and repository name in `%s' remote url `%s'"
                 github-remote remote-url)

      ;; Actual request
      ;; https://developer.github.com/v3/pulls/#create-a-pull-request
      (url-retrieve (format "https://api.github.com/repos/%s/pulls"
                            (match-string 1 remote-url))
                    'github-create-pr-callback (list title) t t))))

(defun github-verify-and-create-pr ()
  (interactive)
  (github-verify-and-execute 'github-create-pr))
